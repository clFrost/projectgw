// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilityTypes.h"
#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "GA_BAse.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTGODSWAR_API UGA_BAse : public UGameplayAbility
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AbilityBase")
	UMaterialInstance* UIMaterial;

	UFUNCTION(BlueprintCallable,Category="AbilityBase")
	FGameplayAbilityInfo GetAbilityInfo();
};
