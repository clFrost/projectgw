// Fill out your copyright notice in the Description page of Project Settings.


#include "GA_BAse.h"

FGameplayAbilityInfo UGA_BAse::GetAbilityInfo()
{
    UGameplayEffect* CooldownEffect = GetCooldownGameplayEffect();
    UGameplayEffect* CostEffect = GetCostGameplayEffect();
    float Cost = 0;
    float CoolDownDuration = 0;
    EAbilityCostType CostType = EAbilityCostType::Mana;
    if(CooldownEffect&&CostEffect)
    {
        CooldownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(1,CoolDownDuration);
    
  
       
        if(CostEffect->Modifiers.Num() > 0)
        {
            FGameplayModifierInfo EffectInfo = CostEffect->Modifiers[0];
            EffectInfo.ModifierMagnitude.GetStaticMagnitudeIfPossible(1,Cost);
            FGameplayAttribute CostAttribute = EffectInfo.Attribute;
            FString AttributeName = CostAttribute.AttributeName;
            if(AttributeName == "Health")
            {
                CostType = EAbilityCostType::Health;
            }
            else if(AttributeName == "mana")
            {
                CostType = EAbilityCostType::Mana;
            }
        }
        return FGameplayAbilityInfo(CoolDownDuration,Cost,CostType,UIMaterial,GetClass());
    }
    return FGameplayAbilityInfo();
}
