// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectGodswarGameMode.h"
#include "ProjectGodswarCharacter.h"
#include "UObject/ConstructorHelpers.h"

AProjectGodswarGameMode::AProjectGodswarGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
