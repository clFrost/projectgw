// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AbilityTypes.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BasePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTGODSWAR_API ABasePlayerController : public APlayerController
{
	GENERATED_BODY()

	public:
		UFUNCTION(BlueprintImplementableEvent,Category="ControllerBase")
		void AddAbilityToUI(FGameplayAbilityInfo AbilityInfo);
};
