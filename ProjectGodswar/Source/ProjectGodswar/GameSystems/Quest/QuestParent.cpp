// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestParent.h"

#include <vector>

// Sets default values
AQuestParent::AQuestParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ParentActor = GetAttachParentActor();

}

void AQuestParent::OrganizeQuestInEditor()
{
	FVector ParentLocation = ParentActor->GetActorLocation();
	this->SetActorLocation(ParentLocation);
}

// Called when the game starts or when spawned
void AQuestParent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AQuestParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

