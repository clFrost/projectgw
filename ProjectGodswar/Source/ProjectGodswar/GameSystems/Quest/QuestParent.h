// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "QuestParent.generated.h"

UCLASS()
class PROJECTGODSWAR_API AQuestParent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AQuestParent();

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=QuestDetails)
	FText Name;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=QuestDetails)
	FText Description;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=QuestRequirements)
	float LevelRequirement;

	UFUNCTION(BlueprintCallable,Category=QuestCleanup)
	void OrganizeQuestInEditor();

	AActor* ParentActor;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
