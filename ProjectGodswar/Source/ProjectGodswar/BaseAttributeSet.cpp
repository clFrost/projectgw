// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAttributeSet.h"

#include "GameplayEffectExtension.h"
#include "GameplayEffect.h"

UBaseAttributeSet::UBaseAttributeSet()
    :health(200.0),
    maxHealth(200.0)
        ,mana(200.0)
    ,maxMana(200.0)
{
}

void UBaseAttributeSet::PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData &Data)
{
    if(Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<UProperty>(UBaseAttributeSet::StaticClass(),GET_MEMBER_NAME_CHECKED(UBaseAttributeSet,health)))
    {
        health.SetCurrentValue(FMath::Clamp(health.GetCurrentValue(),0.f, maxHealth.GetCurrentValue()));
        health.SetBaseValue(FMath::Clamp(health.GetBaseValue(),0.f,maxHealth.GetBaseValue()));
        //UE_LOG(LogTemp, Warning, TEXT("SKIRT, %f"), health.GetCurrentValue());
        OnHealthChanged.Broadcast(health.GetCurrentValue(),maxHealth.GetCurrentValue());
    }
    if(Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<UProperty>(UBaseAttributeSet::StaticClass(),GET_MEMBER_NAME_CHECKED(UBaseAttributeSet,mana)))
    {
        mana.SetCurrentValue(FMath::Clamp(mana.GetCurrentValue(),0.f, maxMana.GetCurrentValue()));
        mana.SetBaseValue(FMath::Clamp(mana.GetBaseValue(),0.f,maxMana.GetBaseValue()));
        UE_LOG(LogTemp, Warning, TEXT("SKIRT, %f"), mana.GetCurrentValue());
        OnManaChanged.Broadcast(mana.GetCurrentValue(),maxMana.GetCurrentValue());
    }
    
}