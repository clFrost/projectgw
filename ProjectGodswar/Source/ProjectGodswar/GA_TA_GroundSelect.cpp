// Fill out your copyright notice in the Description page of Project Settings.


#include "GA_TA_GroundSelect.h"

#include "DrawDebugHelpers.h"
#include "Components/DecalComponent.h"


AGA_TA_GroundSelect::AGA_TA_GroundSelect()
    :Radius(200.0)
{
    PrimaryActorTick.bCanEverTick = true;
    Decal = CreateDefaultSubobject<UDecalComponent>("Decal");
    RootComp = CreateDefaultSubobject<USceneComponent>("RootComp");
    SetRootComponent(RootComp);
    Decal->SetupAttachment(RootComp);
    Decal->DecalSize = FVector(Radius);
}

void AGA_TA_GroundSelect::StartTargeting(UGameplayAbility* Ability)
{
    OwningAbility = Ability;
    MasterPC = Cast<APlayerController>(Ability->GetOwningActorFromActorInfo()->GetInstigatorController());
    Decal->DecalSize = FVector(Radius);
}

void AGA_TA_GroundSelect::ConfirmTargetingAndContinue()
{
    FVector ViewLocation;
    GetPlayerLookingPoint(ViewLocation);

    TArray<FOverlapResult> Overlapped;
    TArray<TWeakObjectPtr<AActor>> OverlappedActors;
    bool TraceComplex = false;
    APawn* MasterPawn = MasterPC->GetPawn();
 

    
    FCollisionQueryParams CollisionQueryParams;
    CollisionQueryParams.bTraceComplex = TraceComplex;
    CollisionQueryParams.bReturnPhysicalMaterial = false;
    if(MasterPawn)
    {
        CollisionQueryParams.AddIgnoredActor(MasterPawn->GetUniqueID());
    }

    bool TryOverlap = GetWorld()->OverlapMultiByObjectType(Overlapped,ViewLocation,
        FQuat::Identity,FCollisionObjectQueryParams(ECC_Pawn),
        FCollisionShape::MakeSphere(Radius),CollisionQueryParams);

    if(TryOverlap)
    {
        for(int32 i = 0; i < Overlapped.Num(); i++)
        {
            APawn* PawnOverrlapped = Cast<APawn>(Overlapped[i].GetActor());
            if(PawnOverrlapped && !OverlappedActors.Contains(PawnOverrlapped) && PawnOverrlapped->ActorHasTag("npc.enemy.attackable"))
            {
                OverlappedActors.Add(PawnOverrlapped);
            }
        }
    }
    //Get center to spawn VFX
    FGameplayAbilityTargetData_LocationInfo* CenterLocation = new FGameplayAbilityTargetData_LocationInfo();
    if(Decal)
    {
        CenterLocation->TargetLocation.LiteralTransform = Decal->GetComponentTransform();
        CenterLocation->TargetLocation.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
    }
   
   
    if(OverlappedActors.Num()>0)
    {
        FGameplayAbilityTargetDataHandle TargetData = StartLocation.MakeTargetDataHandleFromActors(OverlappedActors);
        TargetData.Add(CenterLocation);
        TargetDataReadyDelegate.Broadcast(TargetData);
    }
    else
    {
        TargetDataReadyDelegate.Broadcast(FGameplayAbilityTargetDataHandle(CenterLocation));
    }
}

bool AGA_TA_GroundSelect::GetPlayerLookingPoint(FVector& OutViewPoint)
{
    FVector ViewPoint;
    FRotator ViewRotation;
    MasterPC->GetPlayerViewPoint(ViewPoint,ViewRotation);
    FHitResult HitResult;
    FCollisionQueryParams QueryParams;
    QueryParams.bTraceComplex = true;
    APawn* MasterPawn = MasterPC->GetPawn();
    if(MasterPawn)
    {
        QueryParams.AddIgnoredActor(MasterPawn->GetUniqueID());
    }

    bool TryTrace = GetWorld()->LineTraceSingleByChannel(HitResult,ViewPoint,ViewPoint+ViewRotation.Vector()*10000.0f,ECC_Visibility,QueryParams);
    if(TryTrace)
    {
        OutViewPoint = HitResult.ImpactPoint;
    }
    else
    {
        OutViewPoint = FVector();
    }
    return TryTrace;
}

void AGA_TA_GroundSelect::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
    FVector lookPoint;
    GetPlayerLookingPoint(lookPoint);
    Decal->SetWorldLocation(lookPoint);
}