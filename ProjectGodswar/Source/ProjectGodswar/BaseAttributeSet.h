// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "BaseAttributeSet.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChangedDelegate,float, health, float,maxHealh);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnManaChangedDelegate,float, mana, float,maxMana);
UCLASS()
class PROJECTGODSWAR_API UBaseAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

	public:
		UBaseAttributeSet();
		UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Attribute")
		FGameplayAttributeData health;

		UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Attribute")
		FGameplayAttributeData maxHealth;

		UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Attribute")
	    FGameplayAttributeData mana;

		UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Attribute")
        FGameplayAttributeData maxMana;
		void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData &Data) override;

		UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Faction")
		FGameplayAttributeData FactionID;
		
		FOnHealthChangedDelegate OnHealthChanged;
		FOnManaChangedDelegate OnManaChanged;
};
