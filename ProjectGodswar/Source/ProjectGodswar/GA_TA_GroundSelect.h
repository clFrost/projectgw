// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "Abilities/GameplayAbility.h"
#include "GA_TA_GroundSelect.generated.h"

/**
 * 
 */
class UDecalComponent;
class USceneComponent;
UCLASS()
class PROJECTGODSWAR_API AGA_TA_GroundSelect : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()
	AGA_TA_GroundSelect();
	public:
		UPROPERTY(EditAnywhere,BlueprintReadWrite,meta=(ExponseOnSpawn=True),Category="GroundSelect")
		float Radius;

		virtual void StartTargeting(UGameplayAbility* Ability) override;

		virtual void ConfirmTargetingAndContinue() override;

		virtual void Tick(float DeltaSeconds) override;
		
		UFUNCTION(BlueprintCallable, Category ="GroundSelect")
		bool GetPlayerLookingPoint(FVector& OutViewPoint);

		UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category="GroundSelect")
			UDecalComponent* Decal;

		USceneComponent* RootComp;
};