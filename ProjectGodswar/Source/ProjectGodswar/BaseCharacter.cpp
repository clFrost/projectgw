// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"


#include "AbilityTypes.h"
#include "BasePlayerController.h"
#include "GA_BAse.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
	:IsCurrentlyInteractable(false)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AbilitySystemComp = CreateDefaultSubobject<UAbilitySystemComponent>("AbilitySystemComp");
	BaseAttributeSetComp = CreateDefaultSubobject<UBaseAttributeSet>("BaseAttributeSetComp");
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	BaseAttributeSetComp->OnHealthChanged.AddDynamic(this, &ABaseCharacter::OnHealthChanged);
	BaseAttributeSetComp->OnManaChanged.AddDynamic(this, &ABaseCharacter::OnManaChanged);
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ABaseCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComp;
}

void ABaseCharacter::AquireAbility(TSubclassOf<UGameplayAbility> AbilityToAquire, int levelReq, int strengthReq,
	int intellectReq, int dexReq)
{
	if(AbilitySystemComp)
	{
		if(HasAuthority() && AbilityToAquire)
		{
			AbilitySystemComp->GiveAbility(FGameplayAbilitySpec(AbilityToAquire,0,1));
		}
		AbilitySystemComp->InitAbilityActorInfo(this,this);
	}
}

void ABaseCharacter::AquireAbilitySingleParam(TSubclassOf<UGameplayAbility> AbilityToAquire)
{
	if(AbilitySystemComp)
	{
		if(HasAuthority() && AbilityToAquire)
		{
			AbilitySystemComp->GiveAbility(FGameplayAbilitySpec(AbilityToAquire,0,1));
		}
		AbilitySystemComp->InitAbilityActorInfo(this,this);
	}
}

void ABaseCharacter::AquireMultipleAbilities(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAquire)
{
	for(TSubclassOf<UGameplayAbility> AbilityItem : AbilitiesToAquire)
	{
		AquireAbilitySingleParam(AbilityItem);
		if(AbilityItem->IsChildOf(UGA_BAse::StaticClass()))
		{
			TSubclassOf<UGA_BAse> AbilityBaseClass = *AbilityItem;
			if(AbilityBaseClass!=nullptr)
			{
				AddAbilityToUI(AbilityBaseClass);
			}
		}
	}
}

void ABaseCharacter::OnHealthChanged(float Health, float MaxHealth)
{
	if(Health<=0.0f && !isDead)
	{
		//die
		isDead = true;
		BP_Die();
	}
	BP_OnHealthChanged(Health,MaxHealth);
}

void ABaseCharacter::OnManaChanged(float mana, float MaxMana)
{

	BP_OnManaChanged(mana,MaxMana);
}

void ABaseCharacter::AddAbilityToUI(TSubclassOf<UGA_BAse> abilityToAdd)
{
	ABasePlayerController* BasePlayerController = Cast<ABasePlayerController>(GetController());
	if(BasePlayerController)
	{
		UGA_BAse* AbilityInstance = abilityToAdd.Get()->GetDefaultObject<UGA_BAse>();
		if(AbilityInstance)
		{
			FGameplayAbilityInfo abilityInfo = AbilityInstance->GetAbilityInfo();
			BasePlayerController->AddAbilityToUI(abilityInfo);
		}
	}
	
}


