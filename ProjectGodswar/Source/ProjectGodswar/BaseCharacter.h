// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"
#include "BaseAttributeSet.h"
#include "BaseCharacter.generated.h"

class UBaseAttributeSet;
class UGA_BAse;
UCLASS()
class PROJECTGODSWAR_API ABaseCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category="CharacterBase")
	UAbilitySystemComponent* AbilitySystemComp;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category="CharacterBase")
	UBaseAttributeSet* BaseAttributeSetComp;
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Interaction")
	bool IsCurrentlyInteractable;
	
	UFUNCTION(BlueprintCallable,Category="CharacterBase")
	void AquireAbility(TSubclassOf<UGameplayAbility> AbilityToAquire, int levelReq,int strengthReq,int intellectReq,int dexReq);

	UFUNCTION(BlueprintCallable,Category="CharacterBase")
    void AquireAbilitySingleParam(TSubclassOf<UGameplayAbility> AbilityToAquire);

	UFUNCTION(BlueprintCallable,Category="CharacterBase")
	void AquireMultipleAbilities(TArray<TSubclassOf<UGameplayAbility>> AbilitiesToAquire);

	
	
	UFUNCTION()
	void OnHealthChanged(float Health, float MaxHealth);

	UFUNCTION()
	void OnManaChanged(float mana, float MaxMana);

	UFUNCTION(BlueprintImplementableEvent,Category="CharacterBase",meta=(DisplayName="OnHealthChanged"))
		void BP_OnHealthChanged(float Health, float MaxHealth);

	UFUNCTION(BlueprintImplementableEvent,Category="CharacterBase",meta=(DisplayName="OnManaChanged"))
		void BP_OnManaChanged(float mana, float MaxMana);
	UFUNCTION(BlueprintImplementableEvent,Category="CharacterBase",meta=(DisplayName="Die"))
        void BP_Die();

	protected:
		bool isDead;
	
		void AddAbilityToUI(TSubclassOf<UGA_BAse> abilityToAdd);
};
