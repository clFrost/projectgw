// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectGodswarGameMode.generated.h"

UCLASS(minimalapi)
class AProjectGodswarGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AProjectGodswarGameMode();
};



