// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilityTypes.generated.h"

class UGA_BAse;
UENUM(BlueprintType)
enum class EAbilityCostType : uint8
{
    Health,
    Mana
};
USTRUCT(BlueprintType)
struct FGameplayAbilityInfo
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AbilityInfo")
        float CoolDownDuration;

    UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AbilityInfo")
        float Cost;

    UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AbilityInfo")
        UMaterialInstance* UIMat;
    UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AbilityInfo")
        EAbilityCostType CostType;
    UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AbilityInfo")
        FText Description;
    UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AbilityInfo")
        TSubclassOf<UGA_BAse> AbilityClass;

    
    FGameplayAbilityInfo();
    FGameplayAbilityInfo(float InCooldownDuration,float InCost, EAbilityCostType InCostType, UMaterialInstance* InMaterial, TSubclassOf<UGA_BAse> InAbilityClass);
    
};