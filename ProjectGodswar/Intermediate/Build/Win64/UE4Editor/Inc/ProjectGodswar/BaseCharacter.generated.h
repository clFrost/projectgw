// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UGameplayAbility;
#ifdef PROJECTGODSWAR_BaseCharacter_generated_h
#error "BaseCharacter.generated.h already included, missing '#pragma once' in BaseCharacter.h"
#endif
#define PROJECTGODSWAR_BaseCharacter_generated_h

#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_SPARSE_DATA
#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnManaChanged); \
	DECLARE_FUNCTION(execOnHealthChanged); \
	DECLARE_FUNCTION(execAquireMultipleAbilities); \
	DECLARE_FUNCTION(execAquireAbilitySingleParam); \
	DECLARE_FUNCTION(execAquireAbility);


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnManaChanged); \
	DECLARE_FUNCTION(execOnHealthChanged); \
	DECLARE_FUNCTION(execAquireMultipleAbilities); \
	DECLARE_FUNCTION(execAquireAbilitySingleParam); \
	DECLARE_FUNCTION(execAquireAbility);


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_EVENT_PARMS \
	struct BaseCharacter_eventBP_OnHealthChanged_Parms \
	{ \
		float Health; \
		float MaxHealth; \
	}; \
	struct BaseCharacter_eventBP_OnManaChanged_Parms \
	{ \
		float mana; \
		float MaxMana; \
	};


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_CALLBACK_WRAPPERS
#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABaseCharacter(); \
	friend struct Z_Construct_UClass_ABaseCharacter_Statics; \
public: \
	DECLARE_CLASS(ABaseCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(ABaseCharacter) \
	virtual UObject* _getUObject() const override { return const_cast<ABaseCharacter*>(this); }


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_INCLASS \
private: \
	static void StaticRegisterNativesABaseCharacter(); \
	friend struct Z_Construct_UClass_ABaseCharacter_Statics; \
public: \
	DECLARE_CLASS(ABaseCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(ABaseCharacter) \
	virtual UObject* _getUObject() const override { return const_cast<ABaseCharacter*>(this); }


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABaseCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABaseCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseCharacter(ABaseCharacter&&); \
	NO_API ABaseCharacter(const ABaseCharacter&); \
public:


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseCharacter(ABaseCharacter&&); \
	NO_API ABaseCharacter(const ABaseCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABaseCharacter)


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_PRIVATE_PROPERTY_OFFSET
#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_14_PROLOG \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_EVENT_PARMS


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_RPC_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_CALLBACK_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_INCLASS \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_CALLBACK_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_INCLASS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTGODSWAR_API UClass* StaticClass<class ABaseCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_BaseCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
