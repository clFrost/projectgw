// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FGameplayAbilityInfo;
#ifdef PROJECTGODSWAR_BasePlayerController_generated_h
#error "BasePlayerController.generated.h already included, missing '#pragma once' in BasePlayerController.h"
#endif
#define PROJECTGODSWAR_BasePlayerController_generated_h

#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_SPARSE_DATA
#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_RPC_WRAPPERS
#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_EVENT_PARMS \
	struct BasePlayerController_eventAddAbilityToUI_Parms \
	{ \
		FGameplayAbilityInfo AbilityInfo; \
	};


#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_CALLBACK_WRAPPERS
#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABasePlayerController(); \
	friend struct Z_Construct_UClass_ABasePlayerController_Statics; \
public: \
	DECLARE_CLASS(ABasePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(ABasePlayerController)


#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesABasePlayerController(); \
	friend struct Z_Construct_UClass_ABasePlayerController_Statics; \
public: \
	DECLARE_CLASS(ABasePlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(ABasePlayerController)


#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABasePlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABasePlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABasePlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABasePlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABasePlayerController(ABasePlayerController&&); \
	NO_API ABasePlayerController(const ABasePlayerController&); \
public:


#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABasePlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABasePlayerController(ABasePlayerController&&); \
	NO_API ABasePlayerController(const ABasePlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABasePlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABasePlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABasePlayerController)


#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_PRIVATE_PROPERTY_OFFSET
#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_13_PROLOG \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_EVENT_PARMS


#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_RPC_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_CALLBACK_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_INCLASS \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_CALLBACK_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_INCLASS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTGODSWAR_API UClass* StaticClass<class ABasePlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_BasePlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
