// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTGODSWAR_ProjectGodswarCharacter_generated_h
#error "ProjectGodswarCharacter.generated.h already included, missing '#pragma once' in ProjectGodswarCharacter.h"
#endif
#define PROJECTGODSWAR_ProjectGodswarCharacter_generated_h

#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_SPARSE_DATA
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_RPC_WRAPPERS
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectGodswarCharacter(); \
	friend struct Z_Construct_UClass_AProjectGodswarCharacter_Statics; \
public: \
	DECLARE_CLASS(AProjectGodswarCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(AProjectGodswarCharacter)


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAProjectGodswarCharacter(); \
	friend struct Z_Construct_UClass_AProjectGodswarCharacter_Statics; \
public: \
	DECLARE_CLASS(AProjectGodswarCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(AProjectGodswarCharacter)


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectGodswarCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectGodswarCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectGodswarCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectGodswarCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectGodswarCharacter(AProjectGodswarCharacter&&); \
	NO_API AProjectGodswarCharacter(const AProjectGodswarCharacter&); \
public:


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectGodswarCharacter(AProjectGodswarCharacter&&); \
	NO_API AProjectGodswarCharacter(const AProjectGodswarCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectGodswarCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectGodswarCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectGodswarCharacter)


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AProjectGodswarCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AProjectGodswarCharacter, FollowCamera); }


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_9_PROLOG
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_RPC_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_INCLASS \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_INCLASS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTGODSWAR_API UClass* StaticClass<class AProjectGodswarCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_ProjectGodswarCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
