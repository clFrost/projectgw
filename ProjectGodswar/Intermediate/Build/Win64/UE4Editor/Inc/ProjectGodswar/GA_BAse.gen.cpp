// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProjectGodswar/GA_BAse.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGA_BAse() {}
// Cross Module References
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_UGA_BAse_NoRegister();
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_UGA_BAse();
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_UGameplayAbility();
	UPackage* Z_Construct_UPackage__Script_ProjectGodswar();
	PROJECTGODSWAR_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayAbilityInfo();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstance_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UGA_BAse::execGetAbilityInfo)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FGameplayAbilityInfo*)Z_Param__Result=P_THIS->GetAbilityInfo();
		P_NATIVE_END;
	}
	void UGA_BAse::StaticRegisterNativesUGA_BAse()
	{
		UClass* Class = UGA_BAse::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetAbilityInfo", &UGA_BAse::execGetAbilityInfo },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics
	{
		struct GA_BAse_eventGetAbilityInfo_Parms
		{
			FGameplayAbilityInfo ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GA_BAse_eventGetAbilityInfo_Parms, ReturnValue), Z_Construct_UScriptStruct_FGameplayAbilityInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::Function_MetaDataParams[] = {
		{ "Category", "AbilityBase" },
		{ "ModuleRelativePath", "GA_BAse.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UGA_BAse, nullptr, "GetAbilityInfo", nullptr, nullptr, sizeof(GA_BAse_eventGetAbilityInfo_Parms), Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UGA_BAse_GetAbilityInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UGA_BAse_GetAbilityInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UGA_BAse_NoRegister()
	{
		return UGA_BAse::StaticClass();
	}
	struct Z_Construct_UClass_UGA_BAse_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UIMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UIMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGA_BAse_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameplayAbility,
		(UObject* (*)())Z_Construct_UPackage__Script_ProjectGodswar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UGA_BAse_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UGA_BAse_GetAbilityInfo, "GetAbilityInfo" }, // 1457307340
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGA_BAse_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "GA_BAse.h" },
		{ "ModuleRelativePath", "GA_BAse.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGA_BAse_Statics::NewProp_UIMaterial_MetaData[] = {
		{ "Category", "AbilityBase" },
		{ "ModuleRelativePath", "GA_BAse.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGA_BAse_Statics::NewProp_UIMaterial = { "UIMaterial", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGA_BAse, UIMaterial), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGA_BAse_Statics::NewProp_UIMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGA_BAse_Statics::NewProp_UIMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGA_BAse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGA_BAse_Statics::NewProp_UIMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGA_BAse_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGA_BAse>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGA_BAse_Statics::ClassParams = {
		&UGA_BAse::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UGA_BAse_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UGA_BAse_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGA_BAse_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGA_BAse_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGA_BAse()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGA_BAse_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGA_BAse, 4144022810);
	template<> PROJECTGODSWAR_API UClass* StaticClass<UGA_BAse>()
	{
		return UGA_BAse::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGA_BAse(Z_Construct_UClass_UGA_BAse, &UGA_BAse::StaticClass, TEXT("/Script/ProjectGodswar"), TEXT("UGA_BAse"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGA_BAse);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
