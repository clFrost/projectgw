// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef PROJECTGODSWAR_GA_TA_GroundSelect_generated_h
#error "GA_TA_GroundSelect.generated.h already included, missing '#pragma once' in GA_TA_GroundSelect.h"
#endif
#define PROJECTGODSWAR_GA_TA_GroundSelect_generated_h

#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_SPARSE_DATA
#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetPlayerLookingPoint);


#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetPlayerLookingPoint);


#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGA_TA_GroundSelect(); \
	friend struct Z_Construct_UClass_AGA_TA_GroundSelect_Statics; \
public: \
	DECLARE_CLASS(AGA_TA_GroundSelect, AGameplayAbilityTargetActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(AGA_TA_GroundSelect)


#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAGA_TA_GroundSelect(); \
	friend struct Z_Construct_UClass_AGA_TA_GroundSelect_Statics; \
public: \
	DECLARE_CLASS(AGA_TA_GroundSelect, AGameplayAbilityTargetActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(AGA_TA_GroundSelect)


#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGA_TA_GroundSelect(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGA_TA_GroundSelect) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGA_TA_GroundSelect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGA_TA_GroundSelect); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGA_TA_GroundSelect(AGA_TA_GroundSelect&&); \
	NO_API AGA_TA_GroundSelect(const AGA_TA_GroundSelect&); \
public:


#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGA_TA_GroundSelect(AGA_TA_GroundSelect&&); \
	NO_API AGA_TA_GroundSelect(const AGA_TA_GroundSelect&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGA_TA_GroundSelect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGA_TA_GroundSelect); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGA_TA_GroundSelect)


#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_PRIVATE_PROPERTY_OFFSET
#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_15_PROLOG
#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_RPC_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_INCLASS \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_INCLASS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTGODSWAR_API UClass* StaticClass<class AGA_TA_GroundSelect>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_GA_TA_GroundSelect_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
