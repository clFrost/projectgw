// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProjectGodswar/GA_TA_GroundSelect.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGA_TA_GroundSelect() {}
// Cross Module References
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_AGA_TA_GroundSelect_NoRegister();
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_AGA_TA_GroundSelect();
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_AGameplayAbilityTargetActor();
	UPackage* Z_Construct_UPackage__Script_ProjectGodswar();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UDecalComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AGA_TA_GroundSelect::execGetPlayerLookingPoint)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutViewPoint);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetPlayerLookingPoint(Z_Param_Out_OutViewPoint);
		P_NATIVE_END;
	}
	void AGA_TA_GroundSelect::StaticRegisterNativesAGA_TA_GroundSelect()
	{
		UClass* Class = AGA_TA_GroundSelect::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetPlayerLookingPoint", &AGA_TA_GroundSelect::execGetPlayerLookingPoint },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics
	{
		struct GA_TA_GroundSelect_eventGetPlayerLookingPoint_Parms
		{
			FVector OutViewPoint;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutViewPoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((GA_TA_GroundSelect_eventGetPlayerLookingPoint_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(GA_TA_GroundSelect_eventGetPlayerLookingPoint_Parms), &Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::NewProp_OutViewPoint = { "OutViewPoint", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(GA_TA_GroundSelect_eventGetPlayerLookingPoint_Parms, OutViewPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::NewProp_OutViewPoint,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::Function_MetaDataParams[] = {
		{ "Category", "GroundSelect" },
		{ "ModuleRelativePath", "GA_TA_GroundSelect.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AGA_TA_GroundSelect, nullptr, "GetPlayerLookingPoint", nullptr, nullptr, sizeof(GA_TA_GroundSelect_eventGetPlayerLookingPoint_Parms), Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AGA_TA_GroundSelect_NoRegister()
	{
		return AGA_TA_GroundSelect::StaticClass();
	}
	struct Z_Construct_UClass_AGA_TA_GroundSelect_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Decal_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Decal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGA_TA_GroundSelect_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameplayAbilityTargetActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ProjectGodswar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AGA_TA_GroundSelect_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AGA_TA_GroundSelect_GetPlayerLookingPoint, "GetPlayerLookingPoint" }, // 3076676036
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGA_TA_GroundSelect_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GA_TA_GroundSelect.h" },
		{ "ModuleRelativePath", "GA_TA_GroundSelect.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Decal_MetaData[] = {
		{ "Category", "GroundSelect" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "GA_TA_GroundSelect.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Decal = { "Decal", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGA_TA_GroundSelect, Decal), Z_Construct_UClass_UDecalComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Decal_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Decal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "GroundSelect" },
		{ "ExponseOnSpawn", "TRUE" },
		{ "ModuleRelativePath", "GA_TA_GroundSelect.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGA_TA_GroundSelect, Radius), METADATA_PARAMS(Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Radius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGA_TA_GroundSelect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Decal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGA_TA_GroundSelect_Statics::NewProp_Radius,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGA_TA_GroundSelect_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGA_TA_GroundSelect>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGA_TA_GroundSelect_Statics::ClassParams = {
		&AGA_TA_GroundSelect::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AGA_TA_GroundSelect_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AGA_TA_GroundSelect_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_AGA_TA_GroundSelect_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGA_TA_GroundSelect_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGA_TA_GroundSelect()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGA_TA_GroundSelect_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGA_TA_GroundSelect, 1142909968);
	template<> PROJECTGODSWAR_API UClass* StaticClass<AGA_TA_GroundSelect>()
	{
		return AGA_TA_GroundSelect::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGA_TA_GroundSelect(Z_Construct_UClass_AGA_TA_GroundSelect, &AGA_TA_GroundSelect::StaticClass, TEXT("/Script/ProjectGodswar"), TEXT("AGA_TA_GroundSelect"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGA_TA_GroundSelect);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
