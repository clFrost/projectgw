// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FGameplayAbilityInfo;
#ifdef PROJECTGODSWAR_GA_BAse_generated_h
#error "GA_BAse.generated.h already included, missing '#pragma once' in GA_BAse.h"
#endif
#define PROJECTGODSWAR_GA_BAse_generated_h

#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_SPARSE_DATA
#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAbilityInfo);


#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAbilityInfo);


#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGA_BAse(); \
	friend struct Z_Construct_UClass_UGA_BAse_Statics; \
public: \
	DECLARE_CLASS(UGA_BAse, UGameplayAbility, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(UGA_BAse)


#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGA_BAse(); \
	friend struct Z_Construct_UClass_UGA_BAse_Statics; \
public: \
	DECLARE_CLASS(UGA_BAse, UGameplayAbility, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(UGA_BAse)


#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGA_BAse(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGA_BAse) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGA_BAse); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGA_BAse); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGA_BAse(UGA_BAse&&); \
	NO_API UGA_BAse(const UGA_BAse&); \
public:


#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGA_BAse(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGA_BAse(UGA_BAse&&); \
	NO_API UGA_BAse(const UGA_BAse&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGA_BAse); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGA_BAse); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGA_BAse)


#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_PRIVATE_PROPERTY_OFFSET
#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_13_PROLOG
#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_RPC_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_INCLASS \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_INCLASS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_GA_BAse_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTGODSWAR_API UClass* StaticClass<class UGA_BAse>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_GA_BAse_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
