// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTGODSWAR_QuestParent_generated_h
#error "QuestParent.generated.h already included, missing '#pragma once' in QuestParent.h"
#endif
#define PROJECTGODSWAR_QuestParent_generated_h

#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_SPARSE_DATA
#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOrganizeQuestInEditor);


#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOrganizeQuestInEditor);


#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAQuestParent(); \
	friend struct Z_Construct_UClass_AQuestParent_Statics; \
public: \
	DECLARE_CLASS(AQuestParent, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(AQuestParent)


#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAQuestParent(); \
	friend struct Z_Construct_UClass_AQuestParent_Statics; \
public: \
	DECLARE_CLASS(AQuestParent, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(AQuestParent)


#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AQuestParent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AQuestParent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AQuestParent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AQuestParent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AQuestParent(AQuestParent&&); \
	NO_API AQuestParent(const AQuestParent&); \
public:


#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AQuestParent(AQuestParent&&); \
	NO_API AQuestParent(const AQuestParent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AQuestParent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AQuestParent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AQuestParent)


#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_PRIVATE_PROPERTY_OFFSET
#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_9_PROLOG
#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_RPC_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_INCLASS \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_INCLASS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTGODSWAR_API UClass* StaticClass<class AQuestParent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_GameSystems_Quest_QuestParent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
