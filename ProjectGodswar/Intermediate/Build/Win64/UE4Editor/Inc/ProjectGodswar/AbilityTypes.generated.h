// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTGODSWAR_AbilityTypes_generated_h
#error "AbilityTypes.generated.h already included, missing '#pragma once' in AbilityTypes.h"
#endif
#define PROJECTGODSWAR_AbilityTypes_generated_h

#define ProjectGodswar_Source_ProjectGodswar_AbilityTypes_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics; \
	PROJECTGODSWAR_API static class UScriptStruct* StaticStruct();


template<> PROJECTGODSWAR_API UScriptStruct* StaticStruct<struct FGameplayAbilityInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_AbilityTypes_h


#define FOREACH_ENUM_EABILITYCOSTTYPE(op) \
	op(EAbilityCostType::Health) \
	op(EAbilityCostType::Mana) 

enum class EAbilityCostType : uint8;
template<> PROJECTGODSWAR_API UEnum* StaticEnum<EAbilityCostType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
