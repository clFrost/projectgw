// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProjectGodswar/ProjectGodswarGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProjectGodswarGameMode() {}
// Cross Module References
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_AProjectGodswarGameMode_NoRegister();
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_AProjectGodswarGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_ProjectGodswar();
// End Cross Module References
	void AProjectGodswarGameMode::StaticRegisterNativesAProjectGodswarGameMode()
	{
	}
	UClass* Z_Construct_UClass_AProjectGodswarGameMode_NoRegister()
	{
		return AProjectGodswarGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AProjectGodswarGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AProjectGodswarGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ProjectGodswar,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AProjectGodswarGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ProjectGodswarGameMode.h" },
		{ "ModuleRelativePath", "ProjectGodswarGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AProjectGodswarGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AProjectGodswarGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AProjectGodswarGameMode_Statics::ClassParams = {
		&AProjectGodswarGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AProjectGodswarGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AProjectGodswarGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AProjectGodswarGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AProjectGodswarGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AProjectGodswarGameMode, 73351257);
	template<> PROJECTGODSWAR_API UClass* StaticClass<AProjectGodswarGameMode>()
	{
		return AProjectGodswarGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AProjectGodswarGameMode(Z_Construct_UClass_AProjectGodswarGameMode, &AProjectGodswarGameMode::StaticClass, TEXT("/Script/ProjectGodswar"), TEXT("AProjectGodswarGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AProjectGodswarGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
