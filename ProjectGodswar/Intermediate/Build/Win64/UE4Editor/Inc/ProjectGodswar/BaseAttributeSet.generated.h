// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTGODSWAR_BaseAttributeSet_generated_h
#error "BaseAttributeSet.generated.h already included, missing '#pragma once' in BaseAttributeSet.h"
#endif
#define PROJECTGODSWAR_BaseAttributeSet_generated_h

#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_13_DELEGATE \
struct _Script_ProjectGodswar_eventOnManaChangedDelegate_Parms \
{ \
	float mana; \
	float maxMana; \
}; \
static inline void FOnManaChangedDelegate_DelegateWrapper(const FMulticastScriptDelegate& OnManaChangedDelegate, float mana, float maxMana) \
{ \
	_Script_ProjectGodswar_eventOnManaChangedDelegate_Parms Parms; \
	Parms.mana=mana; \
	Parms.maxMana=maxMana; \
	OnManaChangedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_12_DELEGATE \
struct _Script_ProjectGodswar_eventOnHealthChangedDelegate_Parms \
{ \
	float health; \
	float maxHealh; \
}; \
static inline void FOnHealthChangedDelegate_DelegateWrapper(const FMulticastScriptDelegate& OnHealthChangedDelegate, float health, float maxHealh) \
{ \
	_Script_ProjectGodswar_eventOnHealthChangedDelegate_Parms Parms; \
	Parms.health=health; \
	Parms.maxHealh=maxHealh; \
	OnHealthChangedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_SPARSE_DATA
#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_RPC_WRAPPERS
#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBaseAttributeSet(); \
	friend struct Z_Construct_UClass_UBaseAttributeSet_Statics; \
public: \
	DECLARE_CLASS(UBaseAttributeSet, UAttributeSet, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(UBaseAttributeSet)


#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUBaseAttributeSet(); \
	friend struct Z_Construct_UClass_UBaseAttributeSet_Statics; \
public: \
	DECLARE_CLASS(UBaseAttributeSet, UAttributeSet, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), NO_API) \
	DECLARE_SERIALIZER(UBaseAttributeSet)


#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBaseAttributeSet(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBaseAttributeSet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseAttributeSet); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseAttributeSet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseAttributeSet(UBaseAttributeSet&&); \
	NO_API UBaseAttributeSet(const UBaseAttributeSet&); \
public:


#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBaseAttributeSet(UBaseAttributeSet&&); \
	NO_API UBaseAttributeSet(const UBaseAttributeSet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBaseAttributeSet); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBaseAttributeSet); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UBaseAttributeSet)


#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_PRIVATE_PROPERTY_OFFSET
#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_14_PROLOG
#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_RPC_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_INCLASS \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_INCLASS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTGODSWAR_API UClass* StaticClass<class UBaseAttributeSet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_BaseAttributeSet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
