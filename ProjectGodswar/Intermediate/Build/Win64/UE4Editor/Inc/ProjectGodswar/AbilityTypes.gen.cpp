// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProjectGodswar/AbilityTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAbilityTypes() {}
// Cross Module References
	PROJECTGODSWAR_API UEnum* Z_Construct_UEnum_ProjectGodswar_EAbilityCostType();
	UPackage* Z_Construct_UPackage__Script_ProjectGodswar();
	PROJECTGODSWAR_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayAbilityInfo();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_UGA_BAse_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstance_NoRegister();
// End Cross Module References
	static UEnum* EAbilityCostType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ProjectGodswar_EAbilityCostType, Z_Construct_UPackage__Script_ProjectGodswar(), TEXT("EAbilityCostType"));
		}
		return Singleton;
	}
	template<> PROJECTGODSWAR_API UEnum* StaticEnum<EAbilityCostType>()
	{
		return EAbilityCostType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAbilityCostType(EAbilityCostType_StaticEnum, TEXT("/Script/ProjectGodswar"), TEXT("EAbilityCostType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ProjectGodswar_EAbilityCostType_Hash() { return 3225379234U; }
	UEnum* Z_Construct_UEnum_ProjectGodswar_EAbilityCostType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ProjectGodswar();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAbilityCostType"), 0, Get_Z_Construct_UEnum_ProjectGodswar_EAbilityCostType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAbilityCostType::Health", (int64)EAbilityCostType::Health },
				{ "EAbilityCostType::Mana", (int64)EAbilityCostType::Mana },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Health.Name", "EAbilityCostType::Health" },
				{ "Mana.Name", "EAbilityCostType::Mana" },
				{ "ModuleRelativePath", "AbilityTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ProjectGodswar,
				nullptr,
				"EAbilityCostType",
				"EAbilityCostType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FGameplayAbilityInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PROJECTGODSWAR_API uint32 Get_Z_Construct_UScriptStruct_FGameplayAbilityInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FGameplayAbilityInfo, Z_Construct_UPackage__Script_ProjectGodswar(), TEXT("GameplayAbilityInfo"), sizeof(FGameplayAbilityInfo), Get_Z_Construct_UScriptStruct_FGameplayAbilityInfo_Hash());
	}
	return Singleton;
}
template<> PROJECTGODSWAR_API UScriptStruct* StaticStruct<FGameplayAbilityInfo>()
{
	return FGameplayAbilityInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FGameplayAbilityInfo(FGameplayAbilityInfo::StaticStruct, TEXT("/Script/ProjectGodswar"), TEXT("GameplayAbilityInfo"), false, nullptr, nullptr);
static struct FScriptStruct_ProjectGodswar_StaticRegisterNativesFGameplayAbilityInfo
{
	FScriptStruct_ProjectGodswar_StaticRegisterNativesFGameplayAbilityInfo()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("GameplayAbilityInfo")),new UScriptStruct::TCppStructOps<FGameplayAbilityInfo>);
	}
} ScriptStruct_ProjectGodswar_StaticRegisterNativesFGameplayAbilityInfo;
	struct Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AbilityClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_AbilityClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CostType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CostType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CostType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UIMat_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UIMat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cost_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Cost;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CoolDownDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CoolDownDuration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "AbilityTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FGameplayAbilityInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_AbilityClass_MetaData[] = {
		{ "Category", "AbilityInfo" },
		{ "ModuleRelativePath", "AbilityTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_AbilityClass = { "AbilityClass", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGameplayAbilityInfo, AbilityClass), Z_Construct_UClass_UGA_BAse_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_AbilityClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_AbilityClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "AbilityInfo" },
		{ "ModuleRelativePath", "AbilityTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGameplayAbilityInfo, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CostType_MetaData[] = {
		{ "Category", "AbilityInfo" },
		{ "ModuleRelativePath", "AbilityTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CostType = { "CostType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGameplayAbilityInfo, CostType), Z_Construct_UEnum_ProjectGodswar_EAbilityCostType, METADATA_PARAMS(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CostType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CostType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CostType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_UIMat_MetaData[] = {
		{ "Category", "AbilityInfo" },
		{ "ModuleRelativePath", "AbilityTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_UIMat = { "UIMat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGameplayAbilityInfo, UIMat), Z_Construct_UClass_UMaterialInstance_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_UIMat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_UIMat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Cost_MetaData[] = {
		{ "Category", "AbilityInfo" },
		{ "ModuleRelativePath", "AbilityTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Cost = { "Cost", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGameplayAbilityInfo, Cost), METADATA_PARAMS(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Cost_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Cost_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CoolDownDuration_MetaData[] = {
		{ "Category", "AbilityInfo" },
		{ "ModuleRelativePath", "AbilityTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CoolDownDuration = { "CoolDownDuration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FGameplayAbilityInfo, CoolDownDuration), METADATA_PARAMS(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CoolDownDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CoolDownDuration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_AbilityClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CostType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CostType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_UIMat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_Cost,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::NewProp_CoolDownDuration,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ProjectGodswar,
		nullptr,
		&NewStructOps,
		"GameplayAbilityInfo",
		sizeof(FGameplayAbilityInfo),
		alignof(FGameplayAbilityInfo),
		Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FGameplayAbilityInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FGameplayAbilityInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ProjectGodswar();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("GameplayAbilityInfo"), sizeof(FGameplayAbilityInfo), Get_Z_Construct_UScriptStruct_FGameplayAbilityInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FGameplayAbilityInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FGameplayAbilityInfo_Hash() { return 1923912900U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
