// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROJECTGODSWAR_ProjectGodswarGameMode_generated_h
#error "ProjectGodswarGameMode.generated.h already included, missing '#pragma once' in ProjectGodswarGameMode.h"
#endif
#define PROJECTGODSWAR_ProjectGodswarGameMode_generated_h

#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_SPARSE_DATA
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_RPC_WRAPPERS
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectGodswarGameMode(); \
	friend struct Z_Construct_UClass_AProjectGodswarGameMode_Statics; \
public: \
	DECLARE_CLASS(AProjectGodswarGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), PROJECTGODSWAR_API) \
	DECLARE_SERIALIZER(AProjectGodswarGameMode)


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAProjectGodswarGameMode(); \
	friend struct Z_Construct_UClass_AProjectGodswarGameMode_Statics; \
public: \
	DECLARE_CLASS(AProjectGodswarGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ProjectGodswar"), PROJECTGODSWAR_API) \
	DECLARE_SERIALIZER(AProjectGodswarGameMode)


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PROJECTGODSWAR_API AProjectGodswarGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectGodswarGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PROJECTGODSWAR_API, AProjectGodswarGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectGodswarGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PROJECTGODSWAR_API AProjectGodswarGameMode(AProjectGodswarGameMode&&); \
	PROJECTGODSWAR_API AProjectGodswarGameMode(const AProjectGodswarGameMode&); \
public:


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PROJECTGODSWAR_API AProjectGodswarGameMode(AProjectGodswarGameMode&&); \
	PROJECTGODSWAR_API AProjectGodswarGameMode(const AProjectGodswarGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PROJECTGODSWAR_API, AProjectGodswarGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectGodswarGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectGodswarGameMode)


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_9_PROLOG
#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_RPC_WRAPPERS \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_INCLASS \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_SPARSE_DATA \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_INCLASS_NO_PURE_DECLS \
	ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROJECTGODSWAR_API UClass* StaticClass<class AProjectGodswarGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID ProjectGodswar_Source_ProjectGodswar_ProjectGodswarGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
