// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ProjectGodswar/GameSystems/Quest/QuestParent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeQuestParent() {}
// Cross Module References
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_AQuestParent_NoRegister();
	PROJECTGODSWAR_API UClass* Z_Construct_UClass_AQuestParent();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ProjectGodswar();
// End Cross Module References
	DEFINE_FUNCTION(AQuestParent::execOrganizeQuestInEditor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OrganizeQuestInEditor();
		P_NATIVE_END;
	}
	void AQuestParent::StaticRegisterNativesAQuestParent()
	{
		UClass* Class = AQuestParent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OrganizeQuestInEditor", &AQuestParent::execOrganizeQuestInEditor },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AQuestParent_OrganizeQuestInEditor_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AQuestParent_OrganizeQuestInEditor_Statics::Function_MetaDataParams[] = {
		{ "Category", "QuestCleanup" },
		{ "ModuleRelativePath", "GameSystems/Quest/QuestParent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AQuestParent_OrganizeQuestInEditor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AQuestParent, nullptr, "OrganizeQuestInEditor", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AQuestParent_OrganizeQuestInEditor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AQuestParent_OrganizeQuestInEditor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AQuestParent_OrganizeQuestInEditor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AQuestParent_OrganizeQuestInEditor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AQuestParent_NoRegister()
	{
		return AQuestParent::StaticClass();
	}
	struct Z_Construct_UClass_AQuestParent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelRequirement_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LevelRequirement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AQuestParent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ProjectGodswar,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AQuestParent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AQuestParent_OrganizeQuestInEditor, "OrganizeQuestInEditor" }, // 1806685326
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AQuestParent_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GameSystems/Quest/QuestParent.h" },
		{ "ModuleRelativePath", "GameSystems/Quest/QuestParent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AQuestParent_Statics::NewProp_LevelRequirement_MetaData[] = {
		{ "Category", "QuestRequirements" },
		{ "ModuleRelativePath", "GameSystems/Quest/QuestParent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AQuestParent_Statics::NewProp_LevelRequirement = { "LevelRequirement", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AQuestParent, LevelRequirement), METADATA_PARAMS(Z_Construct_UClass_AQuestParent_Statics::NewProp_LevelRequirement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AQuestParent_Statics::NewProp_LevelRequirement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AQuestParent_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "QuestDetails" },
		{ "ModuleRelativePath", "GameSystems/Quest/QuestParent.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_AQuestParent_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AQuestParent, Description), METADATA_PARAMS(Z_Construct_UClass_AQuestParent_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AQuestParent_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AQuestParent_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "QuestDetails" },
		{ "ModuleRelativePath", "GameSystems/Quest/QuestParent.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_AQuestParent_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AQuestParent, Name), METADATA_PARAMS(Z_Construct_UClass_AQuestParent_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AQuestParent_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AQuestParent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AQuestParent_Statics::NewProp_LevelRequirement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AQuestParent_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AQuestParent_Statics::NewProp_Name,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AQuestParent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AQuestParent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AQuestParent_Statics::ClassParams = {
		&AQuestParent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AQuestParent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AQuestParent_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AQuestParent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AQuestParent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AQuestParent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AQuestParent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AQuestParent, 2504983044);
	template<> PROJECTGODSWAR_API UClass* StaticClass<AQuestParent>()
	{
		return AQuestParent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AQuestParent(Z_Construct_UClass_AQuestParent, &AQuestParent::StaticClass, TEXT("/Script/ProjectGodswar"), TEXT("AQuestParent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AQuestParent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
